CREATE DATABASE tuten;

CREATE DATABASE tuten_tests;

CREATE USER tuten_user WITH PASSWORD 'holatuten123.';

GRANT ALL PRIVILEGES ON DATABASE "tuten" to tuten_user;

GRANT ALL PRIVILEGES ON DATABASE "tuten_tests" to tuten_user;

\c tuten

CREATE OR REPLACE FUNCTION json_intext(text) RETURNS json AS $$ SELECT json_in($1::cstring); $$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION json_invarchar(varchar) RETURNS json AS $$ SELECT json_in($1::cstring); $$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (text AS json) WITH FUNCTION json_intext(text) AS IMPLICIT;

CREATE CAST (varchar AS json) WITH FUNCTION json_invarchar(varchar) AS IMPLICIT;

CREATE OR REPLACE FUNCTION jsonb_intext(text) RETURNS jsonb AS $$ SELECT jsonb_in($1::cstring); $$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION jsonb_invarchar(varchar) RETURNS jsonb AS $$ SELECT jsonb_in($1::cstring); $$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (text AS jsonb) WITH FUNCTION jsonb_intext(text) AS IMPLICIT;

CREATE CAST (varchar AS jsonb) WITH FUNCTION jsonb_invarchar(varchar) AS IMPLICIT;

CREATE EXTENSION unaccent;

CREATE OR REPLACE FUNCTION unaccent(regdictionary, text) RETURNS text LANGUAGE c STABLE STRICT AS '$libdir/unaccent', $function$unaccent_dict$function$;

\c tuten_tests

CREATE OR REPLACE FUNCTION json_intext(text) RETURNS json AS $$ SELECT json_in($1::cstring); $$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION json_invarchar(varchar) RETURNS json AS $$ SELECT json_in($1::cstring); $$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (text AS json) WITH FUNCTION json_intext(text) AS IMPLICIT;

CREATE CAST (varchar AS json) WITH FUNCTION json_invarchar(varchar) AS IMPLICIT;

CREATE OR REPLACE FUNCTION jsonb_intext(text) RETURNS jsonb AS $$ SELECT jsonb_in($1::cstring); $$ LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION jsonb_invarchar(varchar) RETURNS jsonb AS $$ SELECT jsonb_in($1::cstring); $$ LANGUAGE SQL IMMUTABLE;

CREATE CAST (text AS jsonb) WITH FUNCTION jsonb_intext(text) AS IMPLICIT;

CREATE CAST (varchar AS jsonb) WITH FUNCTION jsonb_invarchar(varchar) AS IMPLICIT;

CREATE EXTENSION unaccent;

CREATE OR REPLACE FUNCTION unaccent(regdictionary, text) RETURNS text LANGUAGE c STABLE STRICT AS '$libdir/unaccent', $function$unaccent_dict$function$;

\q
