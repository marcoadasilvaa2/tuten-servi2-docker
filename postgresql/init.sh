#!/bin/bash
set -e

psql -d tuten -Ututen_user -a -f /tmp/sql/dump_create.sql
psql -d tuten -Ututen_user -a -f /tmp/sql/dump_insert.sql
psql -d tuten -Ututen_user -a -f /tmp/sql/functions.sql

psql -d tuten_tests -Ututen_user -a -f /tmp/sql/dump_create.sql
psql -d tuten_tests -Ututen_user -a -f /tmp/sql/dump_insert.sql
psql -d tuten_tests -Ututen_user -a -f /tmp/sql/functions.sql